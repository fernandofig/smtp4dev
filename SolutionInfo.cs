﻿using System;
using System.Reflection;

[assembly: AssemblyCompany("Robert N Wood <rob@rnwood.co.uk>")]
[assembly: AssemblyProduct("smtp4dev")]
[assembly: AssemblyCopyright("Copyright © Robert N Wood 2009-2011")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyVersion("2.1.1")]
[assembly: AssemblyFileVersion("2.1.1")]
